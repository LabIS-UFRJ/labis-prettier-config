# Configuração padrão do Laboratório de Informática e Sociedade para o [prettier](https://github.com/prettier/prettier)

## Instalando em um projeto:

```bash
yarn add -D @labis/prettier-config
```

## Para utilizar, adicione a seguinte linha no arquivo `package.json`:

```json
{
  "prettier": "@labis/prettier-config"
}
```

## Para otimizar a experiência de uso, instale uma extensão do Prettier no seu ambiente de desenvolvimento

