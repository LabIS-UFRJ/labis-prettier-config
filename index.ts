module.exports = {
    printWidth: 100,
    tabWidth: 4,
    jsxSingleQuote: false,
    singleQuote: false,
    quoteProps: "as-needed",
    bracketSpacing: true,
    endOfLine: "auto",
    trailingComma: "none",
    useTabs: false,
    semi: false,
    proseWrap: "preserve",
    arrowParens: "always",
    jsxBracketSameLine: false,
    htmlWhitespaceSensitivity: "css",
    embeddedLanguageFormatting: "auto"
}
